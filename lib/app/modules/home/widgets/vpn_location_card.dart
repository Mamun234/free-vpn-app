import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:free_vpn/app/data/localDb/hiveDbHelper.dart';
import 'package:free_vpn/app/data/models/vpn_info_data.dart';
import 'package:free_vpn/app/modules/home/controllers/home_controller.dart';
import 'package:get/get.dart';

import '../../../core/vpnEngine/vpn_engine.dart';

class VpnLocationCard extends StatelessWidget {
  final VpnInfoData? vpnInfoData;
  const VpnLocationCard({super.key, this.vpnInfoData});

  String formatSpeedBytes(int speedBytes, int decimals) {
    if (speedBytes <= 0) {
      return "0 B";
    }

    const suffixesTilte = ["Bps", "Kbps", "Mbps", "Gbps", "Tbps"];
    var speedTitleIndex = (log(speedBytes) / log(1024)).floor();
    return "${(speedBytes / pow(1024, speedTitleIndex)).toStringAsFixed(decimals)} ${suffixesTilte[speedTitleIndex]}";
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    final homeController = Get.find<HomeController>();
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(vertical: screenSize.height * .01),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      child: InkWell(
        onTap: () {
          homeController.vpnInfoData.value = vpnInfoData!;
          HiveDbHelper.vpnInfoObj = vpnInfoData!;
          Get.back();
          if (homeController.vpnConnectionState.value ==
              VpnEngine.vpnConnectedNow) {
            VpnEngine.stopVpnNow();
            Future.delayed(
                Duration(seconds: 3), () => homeController.connectToVpnNow());
          } else {
            homeController.connectToVpnNow();
          }
        },
        borderRadius: BorderRadius.circular(16),
        child: ListTile(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
          //county flag
          leading: Container(
              padding: const EdgeInsets.all(0.5),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black12),
                  borderRadius: BorderRadius.circular(16)),
              child: Image.asset(
                  "countryFlags/${vpnInfoData?.countryShortName!.toLowerCase()}.png")),
          //country name
          title: Text(vpnInfoData!.countryLongName!),

          //vpn speed
          subtitle: Row(
            children: [
              const Icon(
                Icons.shutter_speed,
                color: Colors.redAccent,
                size: 20,
              ),
              const SizedBox(
                width: 4,
              ),
              Text(
                formatSpeedBytes(vpnInfoData!.speed!, 2),
                style: TextStyle(fontSize: 13),
              ),
            ],
          ),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                vpnInfoData!.vpnSessionsNum!.toString(),
                style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).primaryColorLight),
              ),
              const SizedBox(
                width: 4,
              ),
              const Icon(
                CupertinoIcons.person_2_alt,
                color: Colors.redAccent,
              )
            ],
          ),
        ),
      ),
    );
  }
}
