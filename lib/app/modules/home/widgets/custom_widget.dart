import 'package:flutter/material.dart';

class CustomWidget extends StatelessWidget {
  final String? titleText;
  final String? subTitleText;
  final Widget? roundWidgetWithiconFile;
  const CustomWidget(
      {super.key,
      this.titleText,
      this.subTitleText,
      this.roundWidgetWithiconFile});

  @override
  Widget build(BuildContext context) {
    var sizeScreen = MediaQuery.of(context).size;
    return SizedBox(
      width: sizeScreen.width * .46,
      child: Column(
        children: [
          roundWidgetWithiconFile!,
          const SizedBox(
            height: 7,
          ),
          Text(
            titleText!,
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
          ),
          const SizedBox(
            height: 7,
          ),
          Text(
            subTitleText!,
            style: const TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
          )
        ],
      ),
    );
  }
}
