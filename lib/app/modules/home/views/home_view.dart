import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:free_vpn/app/core/vpnEngine/vpn_engine.dart';
import 'package:free_vpn/app/data/models/vpn_status.dart';
import 'package:get/get.dart';
import '../../../data/localDb/hiveDbHelper.dart';
import '../../../routes/app_pages.dart';
import '../controllers/home_controller.dart';
import '../widgets/custom_widget.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var sizeScreen = MediaQuery.of(context).size;
    VpnEngine.snapshotVpnStage().listen(
      (event) {
        controller.vpnConnectionState.value = event;
      },
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.redAccent,
        title: const Text('Free VPN'),
        centerTitle: true,
        leading: IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.perm_device_info,
              size: 24,
            )),
        actions: [
          IconButton(
              onPressed: () {
                // Get.isDarkMode
                //     ? Get.changeTheme(ThemeData.light())
                //     : Get.changeTheme(ThemeData.dark());

                Get.changeThemeMode(
                    HiveDbHelper.isModeDark ? ThemeMode.light : ThemeMode.dark);
                HiveDbHelper.isModeDark = !HiveDbHelper.isModeDark;

                print(HiveDbHelper.isModeDark);
              },
              icon: const Icon(Icons.brightness_2_outlined))
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          //2 round widget
          //location + ping

          Obx(
            () => Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomWidget(
                  titleText:
                      controller.vpnInfoData.value.countryLongName!.isEmpty
                          ? "Location"
                          : controller.vpnInfoData.value.countryLongName,
                  subTitleText: "free",
                  roundWidgetWithiconFile: CircleAvatar(
                    radius: 32,
                    backgroundColor: Colors.redAccent,
                    backgroundImage: controller
                            .vpnInfoData.value.countryLongName!.isEmpty
                        ? null
                        : AssetImage(
                            "countryFlags/${controller.vpnInfoData.value.countryShortName!.toLowerCase()}.png"),
                    child: controller.vpnInfoData.value.countryLongName!.isEmpty
                        ? const Icon(
                            Icons.flag_circle,
                            size: 30,
                            color: Colors.white,
                          )
                        : null,
                  ),
                ),
                CustomWidget(
                  titleText:
                      controller.vpnInfoData.value.countryLongName!.isEmpty
                          ? "60 ms"
                          : "${controller.vpnInfoData.value.ping!} ms",
                  subTitleText: "Ping",
                  roundWidgetWithiconFile: const CircleAvatar(
                    radius: 32,
                    backgroundColor: Colors.black54,
                    child: Icon(
                      Icons.graphic_eq,
                      size: 30,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),

          // button for vpn cunect
          vpnRoundButton(sizeScreen),

          //2 round widget
          //download + ping
          StreamBuilder<VpnStatus?>(
            initialData: VpnStatus(),
            stream: VpnEngine.snapshotVpnStatus(),
            builder: (context, snapshot) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomWidget(
                    titleText: snapshot.data?.byteIn ?? '0 kbps',
                    subTitleText: "Download",
                    roundWidgetWithiconFile: const CircleAvatar(
                      radius: 32,
                      backgroundColor: Colors.green,
                      child: Icon(
                        Icons.arrow_circle_down,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  CustomWidget(
                    titleText: snapshot.data?.byteOut ?? '0 kbps',
                    subTitleText: "Upload",
                    roundWidgetWithiconFile: const CircleAvatar(
                      radius: 32,
                      backgroundColor: Colors.purpleAccent,
                      child: Icon(
                        Icons.arrow_circle_up_rounded,
                        size: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              );
            },
          )
        ],
      ),
      bottomNavigationBar: locationSelectionBottomNavigation(context),
    );
  }

  Widget vpnRoundButton(sizeScreen) {
    return Column(
      children: [
        Semantics(
          button: true,
          child: InkWell(
            onTap: () {},
            borderRadius: BorderRadius.circular(100),
            child: Container(
              padding: EdgeInsets.all(18),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: controller.getRoundVpnButtonColor.withOpacity(.1)),
              child: Container(
                padding: EdgeInsets.all(18),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: controller.getRoundVpnButtonColor.withOpacity(.1)),
                child: Container(
                  height: sizeScreen.height * .14,
                  width: sizeScreen.height * .14,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: controller.getRoundVpnButtonColor),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.power_settings_new,
                        size: 30,
                        color: Colors.white,
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(
                        controller.getRoundVpnButtonText,
                        style: const TextStyle(
                            fontSize: 13,
                            color: Colors.white,
                            fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  locationSelectionBottomNavigation(BuildContext context) {
    return SafeArea(
        child: Semantics(
      button: true,
      child: InkWell(
        onTap: () {
          Get.toNamed(Routes.HOME + Routes.availableVpnServerLocationView);
        },
        child: Container(
          color: Colors.redAccent,
          padding: const EdgeInsets.symmetric(horizontal: 8),
          height: 62,
          child: const Row(
            children: [
              Icon(
                CupertinoIcons.flag_circle,
                color: Colors.white,
                size: 36,
              ),
              SizedBox(
                width: 12,
              ),
              Text(
                "Select Country / Location",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w400),
              ),
              Spacer(),
              CircleAvatar(
                backgroundColor: Colors.white,
                child: Icon(
                  Icons.keyboard_arrow_right,
                  color: Colors.redAccent,
                  size: 26,
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }
}
