import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/home_controller.dart';
import '../widgets/vpn_location_card.dart';

class AvailableVpnServerLocationView extends GetView<HomeController> {
  const AvailableVpnServerLocationView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (controller.vpnFreeServersAvailableListServersList.isEmpty) {
      controller.retriveVpnInformation();
    }

    return Obx(() => Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.redAccent,
            title: Text(
                "VPN Location(${controller.vpnFreeServersAvailableListServersList.length})"),
          ),
          body: controller.isLoadingNewLocations.value
              ? const Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.redAccent),
                  ),
                )
              : controller.vpnFreeServersAvailableListServersList.isEmpty
                  ? const Center(
                      child: Text(
                      "No VPNs Found,Try Again.",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black54,
                          fontWeight: FontWeight.bold),
                    ))
                  : Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: ListView.builder(
                          itemCount: controller
                              .vpnFreeServersAvailableListServersList.length,
                          physics: const BouncingScrollPhysics(),
                          padding: const EdgeInsets.all(3),
                          itemBuilder: (context, index) {
                            return VpnLocationCard(
                              vpnInfoData: controller
                                      .vpnFreeServersAvailableListServersList[
                                  index],
                            );
                          }),
                    ),
          floatingActionButton: Padding(
            padding: const EdgeInsets.only(bottom: 10, right: 10),
            child: FloatingActionButton(
              backgroundColor: Colors.redAccent,
              onPressed: () {
                controller.retriveVpnInformation();
              },
              child: const Icon(
                CupertinoIcons.refresh_circled,
                size: 40,
              ),
            ),
          ),
        ));
  }
}
