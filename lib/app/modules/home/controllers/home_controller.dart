import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:free_vpn/app/core/vpnEngine/vpn_engine.dart';
import 'package:free_vpn/app/data/models/vpn_configuration.dart';
import 'package:free_vpn/app/data/models/vpn_info_data.dart';
import 'package:free_vpn/app/data/services/vpn_api_gate.dart';
import 'package:get/get.dart';
import '../../../data/localDb/hiveDbHelper.dart';

class HomeController extends GetxController {
  final Rx<VpnInfoData> vpnInfoData = HiveDbHelper.vpnInfoObj.obs;

  final vpnConnectionState = VpnEngine.vpnDisConnectedNow.obs;

  @override
  void onInit() {
    // retriveVpnInformation();
    super.onInit();
  }

  void connectToVpnNow() async {
    if (vpnInfoData.value.base64OpenVpnConfigurationData!.isEmpty) {
      Get.snackbar(
          "Country / Location", "Please select Country / Location first");
      return;
    }
//Disconnected
    if (vpnConnectionState.value == VpnEngine.vpnDisConnectedNow) {
      final dataConfigVpn = Base64Decoder()
          .convert(vpnInfoData.value.base64OpenVpnConfigurationData!);

      final configuration = Utf8Decoder().convert(dataConfigVpn);

      final vpnConfiguration = VpnConfiguration(
          userName: "vpn",
          password: "vpn",
          countryName: vpnInfoData.value.countryLongName,
          config: configuration);

      await VpnEngine.startVpnNow(vpnConfiguration);
    } else {
      await VpnEngine.stopVpnNow();
    }
  }

  Color get getRoundVpnButtonColor {
    switch (vpnConnectionState.value) {
      case VpnEngine.vpnDisConnectedNow:
        return Colors.redAccent;

      case VpnEngine.vpnConnectedNow:
        return Colors.green;

      default:
        return Colors.orangeAccent;
    }
  }

  String get getRoundVpnButtonText {
    switch (vpnConnectionState.value) {
      case VpnEngine.vpnDisConnectedNow:
        return "Tap to connect";

      case VpnEngine.vpnConnectedNow:
        return "Disconnect";

      default:
        return "Connection....";
    }
  }

  //controllerVpnLocation
  List<VpnInfoData> vpnFreeServersAvailableListServersList =
      HiveDbHelper.vpnList;
  final RxBool isLoadingNewLocations = false.obs;

  Future<void> retriveVpnInformation() async {
    isLoadingNewLocations.value = true;
    vpnFreeServersAvailableListServersList =
        await ApiVpnGate.retriveAllAvailableFreeVpnServers();
    isLoadingNewLocations.value = false;
  }
}
