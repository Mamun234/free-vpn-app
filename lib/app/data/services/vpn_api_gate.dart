import 'dart:convert';
import 'package:csv/csv.dart';
import 'package:flutter/material.dart';
import 'package:free_vpn/app/data/models/vpn_info_data.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import '../localDb/hiveDbHelper.dart';
import '../models/ip_info.dart';

class ApiVpnGate {
  static Future<List<VpnInfoData>> retriveAllAvailableFreeVpnServers() async {
    final List<VpnInfoData> vpnServerList = [];

    try {
      final respondeFromApi =
          await http.get(Uri.parse("https://www.vpngate.net/api/iphone/"));

      final commaSeparatedValueString =
          respondeFromApi.body.split("#")[1].replaceAll("*", "");

      List<List<dynamic>> listData =
          const CsvToListConverter().convert(commaSeparatedValueString);

      final header = listData[0];

      for (int counter = 1; counter < listData.length - 1; counter++) {
        Map<String, dynamic> jsonData = {};
        for (int innerCounter = 0;
            innerCounter < header.length;
            innerCounter++) {
          jsonData.addAll({
            header[innerCounter].toString(): listData[counter][innerCounter]
          });
        }
        vpnServerList.add(VpnInfoData.fromJson(jsonData));
      }
    } catch (errorMsg) {
      Get.snackbar("Error Occurred", errorMsg.toString(),
          colorText: Colors.white,
          backgroundColor: Colors.redAccent.withOpacity(.8));
    }

    vpnServerList.shuffle();

    if (vpnServerList.isNotEmpty) {
      HiveDbHelper.vpnList = vpnServerList;
    }
    return vpnServerList;
  }

  static Future<void> retrieveIPDetails(
      {required Rx<IpInfo> ipInformation}) async {
    try {
      final responseFromApi =
          await http.get(Uri.parse("http://ip-api.com/json/"));
      final dataFromApi = jsonDecode(responseFromApi.body);
      ipInformation.value = IpInfo.fromJson(dataFromApi);
    } catch (errorMsg) {
      Get.snackbar("Error Occurred", errorMsg.toString(),
          colorText: Colors.white,
          backgroundColor: Colors.redAccent.withOpacity(.8));
    }
  }
}
