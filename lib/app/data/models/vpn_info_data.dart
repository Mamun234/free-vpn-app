import 'dart:convert';

VpnInfoData vpnInfoDataFromJson(String str) =>
    VpnInfoData.fromJson(json.decode(str));

String vpnInfoDataToJson(VpnInfoData data) => json.encode(data.toJson());

class VpnInfoData {
  String? hostName;
  final String? ip;
  final int? ping;
  final int? speed;
  String? countryLongName;
  String? countryShortName;
  final int? vpnSessionsNum;
  final String? base64OpenVpnConfigurationData;

  VpnInfoData({
    this.hostName,
    this.ip,
    this.ping,
    this.speed,
    this.countryLongName,
    this.countryShortName,
    this.vpnSessionsNum,
    this.base64OpenVpnConfigurationData,
  });

  factory VpnInfoData.fromJson(Map<String, dynamic> json) => VpnInfoData(
        hostName: json["HostName"] ?? "",
        ip: json["IP"] ?? "",
        ping: json["Ping"] ?? 0,
        speed: json["Speed"] ?? 0,
        countryLongName: json["CountryLong"] ?? "",
        countryShortName: json["CountryShort"] ?? "",
        vpnSessionsNum: json["NumVpnSessions"] ?? 0,
        base64OpenVpnConfigurationData: json["OpenVPN_ConfigData_Base64"] ?? "",
      );

  Map<String, dynamic> toJson() => {
        "HostName": hostName,
        "IP": ip,
        "Ping": ping,
        "Speed": speed,
        "CountryLong": countryLongName,
        "CountryShort": countryShortName,
        "NumVpnSessions": vpnSessionsNum,
        "OpenVPN_ConfigData_Base64": base64OpenVpnConfigurationData,
      };
}
