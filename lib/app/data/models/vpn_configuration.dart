// import 'dart:convert';

// VpnConfiguration vpnConfigurationFromJson(String str) =>
//     VpnConfiguration.fromJson(json.decode(str));

// String vpnConfigurationToJson(VpnConfiguration data) =>
//     json.encode(data.toJson());

class VpnConfiguration {
  final String? userName;
  final String? password;
  final String? countryName;
  final String? config;

  VpnConfiguration({
    required this.userName,
    required this.password,
    required this.countryName,
    required this.config,
  });

  // factory VpnConfiguration.fromJson(Map<String, dynamic> json) =>
  //     VpnConfiguration(
  //       userName: json["userName"],
  //       password: json["password"],
  //       countryName: json["countryName"],
  //       config: json["config"],
  //     );

  // Map<String, dynamic> toJson() => {
  //       "userName": userName,
  //       "password": password,
  //       "countryName": countryName,
  //       "config": config,
  //     };
}
