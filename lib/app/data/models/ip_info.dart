import 'dart:convert';

IpInfo ipInfoFromJson(String str) => IpInfo.fromJson(json.decode(str));

String ipInfoToJson(IpInfo data) => json.encode(data.toJson());

class IpInfo {
  late final String? countryName;
  late final String? regionName;
  late final String? cityName;
  late final String? zipCode;
  late final String? timeZone;
  late final String? internetServiceProvider;
  late final String? query;

  IpInfo({
    this.countryName,
    this.regionName,
    this.cityName,
    this.zipCode,
    this.timeZone,
    this.internetServiceProvider,
    this.query,
  });

  factory IpInfo.fromJson(Map<String, dynamic> json) => IpInfo(
        countryName: json["countryName"],
        regionName: json["regionName"] ?? "",
        cityName: json["cityName"] ?? "",
        zipCode: json["zipCode"] ?? "",
        timeZone: json["timeZone"] ?? "Unknown",
        internetServiceProvider: json["internetServiceProvider"] ?? "Unknown",
        query: json["query"] ?? "Not available",
      );

  Map<String, dynamic> toJson() => {
        "countryName": countryName,
        "regionName": regionName,
        "cityName": cityName,
        "zipCode": zipCode,
        "timeZone": timeZone,
        "internetServiceProvider": internetServiceProvider,
        "query": query,
      };
}
