import 'dart:convert';
import 'package:free_vpn/app/data/models/vpn_info_data.dart';
import 'package:hive_flutter/hive_flutter.dart';

class HiveDbHelper {
  static late Box boxOfData;

  static Future<void> initHive() async {
    await Hive.initFlutter();
    boxOfData = await Hive.openBox("data");
  }

  //saving user choice about theme selection
  static bool get isModeDark => boxOfData.get("isModeDark") ?? false;
  static set isModeDark(bool value) => boxOfData.put("isModeDark", value);

  //for saving single selected vpn details
  static VpnInfoData get vpnInfoObj =>
      VpnInfoData.fromJson(jsonDecode(boxOfData.get("vpn") ?? "{}"));

  static set vpnInfoObj(VpnInfoData value) =>
      boxOfData.put("vpn", jsonEncode(value));

  //for saveing all vpn servers details
  static List<VpnInfoData> get vpnList {
    List<VpnInfoData> tempVpnList = [];
    final dataVpn = jsonDecode(boxOfData.get("vpnList") ?? '[]');

    for (var data in dataVpn) {
      tempVpnList.add(VpnInfoData.fromJson(data));
    }

    return tempVpnList;
  }

  static set vpnList(List<VpnInfoData> valueList) =>
      boxOfData.put("vpnList", jsonEncode(valueList));
}
