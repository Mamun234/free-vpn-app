import 'dart:convert';
import 'dart:developer';
// import 'package:flutter/services.dart';
// import 'package:free_vpn/app/data/models/vpn_configuration.dart';
// import 'package:free_vpn/app/data/models/vpn_status.dart';

import 'package:flutter/services.dart';

import '../../data/models/vpn_configuration.dart';
import '../../data/models/vpn_status.dart';

class VpnEngine {
  //native channel
  static const String eventChannelVpnStage = "vpnStage";
  static const String eventChannelVpnStatus = "vpnStatus";
  static const String methodChannelVpnControl = "vpnControl";

  //vpn connection stage snapshot
  static Stream<String> snapshotVpnStage() =>
      EventChannel(eventChannelVpnStage).receiveBroadcastStream().cast();

  //vpn connection status snapshot
  static Stream<VpnStatus?> snapshotVpnStatus() =>
      EventChannel(eventChannelVpnStatus)
          .receiveBroadcastStream()
          .map((eventStatus) => VpnStatus.fromJson(jsonDecode(eventStatus)))
          .cast();

  //vpn start
  static Future<void> startVpnNow(VpnConfiguration vpnConfiguration) async {
    log(vpnConfiguration.config!);
    return MethodChannel(methodChannelVpnControl).invokeMethod(
      "start",
      {
        "config": vpnConfiguration.config,
        "country": vpnConfiguration.countryName,
        "userName": vpnConfiguration.userName,
        "password": vpnConfiguration.password,
      },
    );
  }

  //vpn stop
  static Future<void> stopVpnNow() {
    return MethodChannel(methodChannelVpnControl).invokeMethod("stop");
  }

  static Future<void> killSwitchOpenNow() {
    return MethodChannel(methodChannelVpnControl).invokeMethod("kill_switch");
  }

  static Future<void> refreshStageNow() {
    return MethodChannel(methodChannelVpnControl).invokeMethod("refresh");
  }

  static Future<String?> getStageNow() {
    return MethodChannel(methodChannelVpnControl).invokeMethod("stage");
  }

  static Future<bool> inConnecteNow() {
    return getStageNow()
        .then((valueStage) => valueStage!.toLowerCase() == "connected");
  }

  //stages of vpn connectec
  static const String vpnConnectedNow = "connected";
  static const String vpnDisConnectedNow = "disconnected";
  static const String vpnWaitConnectionNow = "wait_connection";
  static const String vpnAuthenticationNow = "authentication";
  static const String vpnReconnectionNow = "reconnect";
  static const String vpnNoConnectionNow = "no_connection";
  static const String vpnConnectingNow = "connedting";
  static const String vpnPrepareNow = "prepare";
  static const String vpnDenieNow = "denied";
}
