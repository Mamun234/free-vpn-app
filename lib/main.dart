import 'package:flutter/material.dart';
import 'package:free_vpn/app/data/localDb/hiveDbHelper.dart';
import 'package:get/get.dart';
import 'app/routes/app_pages.dart';

//base project
//https://github.com/codingcafe1/vpn_app
//https://www.vpngate.net/en/

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await HiveDbHelper.initHive();
  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Free Vpn",
      themeMode: HiveDbHelper.isModeDark ? ThemeMode.dark : ThemeMode.light,
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
    ),
  );
}

// extension AppTheme on ThemeData {
//   Color get lightTextColor =>
//       HiveDbHelper.isModeDark ? Colors.white70 : Colors.black54;

//   Color get bottomNavigationColor =>
//       HiveDbHelper.isModeDark ? Colors.white12 : Colors.redAccent;
// }
